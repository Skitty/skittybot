'use strict';

const path = require('path');
const fs = require('fs');

class plug_man {
    constructor() {
        this.onunload_function_cache = new Map();
        this.onguildleave_function_cache = [];
        this.dependencies = new Map();
        this.list = new Map();
        this.nameToPath = new Map();
        this.debugMessages = [];
    }

    consoleCollector(...args) {
        this.debugMessages.push(args.join(' '));
    }

    async hotReloadModule(msg, args, all = false) {
        // TODO: enable a way to load only new modules
        let target = args.join(' ');
        let titles = Array.from(this.list.keys());

        let isCapturing = false;
        if (SkittyBot.logger.setCaptureFunction) {
            isCapturing = true;
            this.debugMessages = [];
            SkittyBot.logger.setCaptureFunction(this.consoleCollector.bind(this));
        }

        switch (all) {
            case true: {
                msg.channel.send(`Reloading all modules...`);
                await titles.reduce((p, title) => p.then(async () => {
                    await new Promise(async r => {
                        let res = await this.fullUnloadPlugin(title);
                        if (!res) SkittyBot.logger.error(`ERROR UNLOADING MODULE [${title}]`);
                        r();
                    });
                }), Promise.resolve());
                // trigger a full reload of everything, including anything new that might have been added
                this.loadPlugins();
                break;
            }
            case false: {
                if (!titles.includes(target)) {
                    msg.channel.send(`Target "${target}" not matched in module registry.`);
                    return;
                }
                msg.channel.send(`Reloading ${target}`);
                await this.reloadPlugin({ plugin_title: target }, true);
            }
        }

        if (isCapturing) {
            SkittyBot.logger.stopCapture();
            let messages = Array.from(this.debugMessages);
            delete this.debugMessages;
            let sections = [];
            let sIndex = 0;
            let sLen = 0;
            let cMsg;
            let concat = '';
            for (let i = 0; i < messages.length; i++) {
                cMsg = `${messages[i]}\n`;
                // if a console message is ridiculously long ignore it
                if (cMsg.length > 1900) {
                    cMsg = `!!! >>> MESSAGE TOO LARGE TO INCLUDE, CHECK CONSOLE FOR MORE INFORMATION <<< !!!`;
                }
                if ((sLen + cMsg.length) > 1900) {
                    sections[sIndex++] = concat;
                    concat = '';
                    sLen = 0;
                }
                sLen += cMsg.length;
                concat += cMsg;
            }
            if (concat.length) sections[sIndex++] = concat;
            sections.forEach(chunk => msg.channel.send(`\`\`\`\n${chunk}\`\`\``));
        }
    }

    async fullUnloadPlugin(plugin_title) {
        let unload = this.onunload_function_cache.get(plugin_title);
        if (unload instanceof Function) {
            try {
                if (unload.constructor.name === 'AsyncFunction') {
                    await unload();
                } else {
                    unload();
                }
            } catch (e) {
                SkittyBot.logger.reportError(e);
                // don't continue if there was an error unless an override was enabled
                if (SkittyBot.DEVOVERRIDE !== 0xA5501) return false;
                SkittyBot.logger.warn(`ERROR ENCOUNTERED WHILE UNLOADING MODULE [${plugin_title}]`);
                SkittyBot.logger.warn(`DEVELOPER OVERRIDE ENABLED: CONTINUING ANYWAY`);
            } finally {
                // remove reference to unload function
                this.onunload_function_cache.delete(plugin_title);
            }
        }
        let cfg = this.list.get(plugin_title);
        if (cfg) {
            // clear all command and alias references for a full reload
            if (Array.isArray(cfg.commands)) {
                cfg.commands.forEach(cmd => {
                    if (Array.isArray(cmd.aliases)) {
                        cmd.aliases.forEach(a => SkittyBot.command_alias.delete(a));
                    } else if (typeof cmd.aliases === 'string') {
                        SkittyBot.command_alias.delete(cmd.aliases);
                    }
                    SkittyBot.commands.delete(cmd.command);
                });
            }
            this.list.delete(plugin_title);
        }
        this.clearCommandCacheForTitle(plugin_title);
        let p = this.nameToPath.get(plugin_title);
        if (p) {
            delete require.cache[require.resolve(p)];
        } else {
            SkittyBot.logger.error(`ERROR: FAILED TO REMOVE [${plugin_title}] FROM MODULE CACHE.`);
            SkittyBot.logger.error(`ERROR: THIS MOST LIKELY MEANS THE MODULE WILL NOT RELOAD.`);
        }
        this.nameToPath.delete(plugin_title);
        return true;
    }

    async reloadPlugin(req, reload = true) {
        let unload = this.onunload_function_cache.get(req.plugin_title);
        if (unload instanceof Function) {
            try {
                if (unload.constructor.name === 'AsyncFunction') {
                    await unload();
                } else {
                    unload();
                }
            } catch (e) {
                SkittyBot.logger.reportError(e);
                // don't continue if there was an error unless an override was enabled
                if (SkittyBot.DEVOVERRIDE !== 0xA5501) return false;
                SkittyBot.logger.warn(`ERROR ENCOUNTERED WHILE UNLOADING MODULE [${req.plugin_title}]`);
                SkittyBot.logger.warn(`DEVELOPER OVERRIDE ENABLED: CONTINUING ANYWAY`);
            } finally {
                // remove reference to unload function
                this.onunload_function_cache.delete(req.plugin_title);
            }
        }
        let cfg = this.list.get(req.plugin_title);
        if (reload && cfg) {
            // If we're reloading the thing, clear this cache
            // Otherwise keep it so we can set disabled flags that prompts the bot system to tell people it's not working momentarily
            if (Array.isArray(cfg.commands)) {
                cfg.commands.forEach(cmd => {
                    if (Array.isArray(cmd.aliases)) {
                        cmd.aliases.forEach(a => SkittyBot.command_alias.delete(a));
                    } else if (typeof cmd.aliases === 'string') {
                        SkittyBot.command_alias.delete(cmd.aliases);
                    }
                    SkittyBot.commands.delete(cmd.command);
                });
            }
        }
        this.clearCommandCacheForTitle(req.plugin_title);
        // delete require cache so module is garbage collected
        let p = this.nameToPath.get(req.plugin_title);
        if (p) delete require.cache[require.resolve(p)];
        if (reload) {
            let location = cfg.path;
            if (!location) location = this.nameToPath.get(req.plugin_title);
            if (!location) {
                SkittyBot.logger.error(`IRREMEDIABLE ERROR: UNABLE TO RESOLVE [${req.plugin_title}] TO A LOCATION!`);
                return false;
            }
            try {
                require(location);
            } catch (e) {
                SkittyBot.logger.reportError(e);
                return false;
            }
        }
        return true;
    }

    clearCommandCacheForTitle(title) {
        // just in case: lets go right through the fucking command registry and annihilate anything that might have been left over LMFAO
        SkittyBot.commands.forEach((cmd, key) => {
            if (!cmd.plugin_title) {
                SkittyBot.logger.error(`clearCommandCacheForTitle: command object has no plugin_title property!`);
                console.error(cmd);
            }
            if (cmd.plugin_title === title) {
                if (cmd.aliases) {
                    if (Array.isArray(cmd.aliases) && cmd.aliases.length) {
                        cmd.aliases.forEach(alias => {
                            SkittyBot.command_alias.delete(alias);
                        });
                    } else if (typeof cmd.aliases === 'string') {
                        // it should never be a string, but... just in case...
                        SkittyBot.command_alias.delete(cmd.aliases);
                    } else if (cmd.aliases) {
                        console.error(`Unexpected value for command aliases:`, cmd.aliases);
                    }
                }
                SkittyBot.commands.delete(key);
            }
        });
    }

    async controller(req) {
        let plugin = this.nameToPath.get(req.plugin_title);
        let pkg;
        try {
            pkg = require(path.join(plugin, 'package.json'));
        } catch (e) {
            SkittyBot.logger.reportError(e);
            return false;
        }
        pkg.disabled = !req.enabled;
        fs.writeFileSync(path.join(plugin, 'package.json'), `${JSON.stringify(pkg, null, 2)}\n`);
        await this.reloadPlugin(req, req.enabled);
        let cfg = this.list.get(req.plugin_title);
        cfg.enabled = req.enabled;
        if (pkg.disabled) {
            // plugin was disabled and its command objects still exist so block them
            if (Array.isArray(cfg.commands)) {
                cfg.commands.forEach(cmd => {
                    let command = SkittyBot.commands.get(cmd.command);
                    if (command) command.enabled = false;
                });
            }
        }
        return true;
    }

    addToList(cfg) {
        let detached = JSON.parse(JSON.stringify(cfg));
        this.list.set(cfg.plugin_title, detached);
        this.nameToPath.set(cfg.plugin_title, cfg.path);
    }

    async _createCommands(config) {

        let commands = config.commands;
        let name = config.plugin_title;
        // only show relative path
        let folder = `${path.relative(process.cwd(), config.path)}`.replace(/\\/g, '/');
        let c_count = 0;
        let a_count = 0;

        if (!config.enabled) {
            this.addToList(config);
            SkittyBot.logger.info(`Skipping disabled module [${name}] from [${folder}]`);
            return false;
        }

        SkittyBot.logger.info(`Loading [${name}] command module from [${folder}]`);

        // call the onload functions in case they exist
        if (config.onload instanceof Function) {
            try {
                if (config.onload.constructor.name === 'AsyncFunction') {
                    await config.onload();
                } else {
                    config.onload();
                }
            } catch (e) {
                SkittyBot.logger.reportError(e);
                return false;
            }
        }

        this.addToList(config);

        // Some modules can populate the commands array after the onload function
        // "commands" here is a reference variable to the modules commands array
        // modules can potentially contain no commands and handle messages or events instead, so do not throw an error when there are no commands
        if (!Array.isArray(commands)) {
            SkittyBot.logger.info(`Module  [${name}]: commands is not an array`);
        } else if (Array.isArray(commands) && !commands.length) {
            SkittyBot.logger.info(`Module  [${name}]: commands had no command definitions`);
        } else if (Array.isArray(commands) && commands.length) {
            SkittyBot.logger.info(`Module  [${name}] processing command definitions`);
            for (let cmd of commands) {
                if (!SkittyBot.commands.has(cmd.command)) {

                    SkittyBot.commands.set(cmd.command, cmd);
                    // everything of cmd object in SkittyBot.commands is a reference, modification anywhere appears everywhere
                    // add plugin title as is to the command object so we can match it to a plugin elsewhere...
                    cmd.plugin_title = name;
                    ++c_count;

                    SkittyBot.logger.verbose(`Module  [${name}] created command [${cmd.command}]`);
                    if (cmd.aliases && !Array.isArray(cmd.aliases)) cmd.aliases = [cmd.aliases];
                    if (Array.isArray(cmd.aliases)) {

                        cmd.aliases.forEach(alias => {

                            if (SkittyBot.commands.has(alias)) {
                                // unfortunately we can't just n*'#' in ECMAScript as you would in python
                                let msg = [
                                    '',
                                    `###################################################################################`,
                                    `!> Module  [${name}] attempted to over-write existing command with a command alias!`,
                                    `!> \tALIAS : ${alias}`,
                                    `!> \tPLUGIN: ${folder}`,
                                    `###################################################################################`,
                                ];
                                SkittyBot.logger.warn(msg.join('\n'));
                                return;
                            }

                            if (SkittyBot.command_alias.has(alias)) {
                                let msg = [
                                    '',
                                    `####################################################################`,
                                    `!> Module  [${name}] attempted to over-write existing command alias!`,
                                    `!> \tALIAS : ${alias}`,
                                    `!> \tPLUGIN: ${folder}`,
                                    `####################################################################`,
                                ];
                                SkittyBot.logger.warn(msg.join('\n'));
                                return;
                            }

                            SkittyBot.command_alias.set(alias, cmd.command);
                            SkittyBot.logger.verbose(`Module  [${name}] created alias [${alias}]`);

                            ++a_count;

                        });

                    }

                } else {
                    let msg = [
                        '',
                        `#############################################################`,
                        `!> Module  [${name}] attempted to overwrite existing command!`,
                        `!> \tCOMMAND: ${cmd.command}`,
                        `!> \tPLUGIN : ${folder}`,
                        `#############################################################`
                    ];
                    SkittyBot.logger.error(msg.join('\n'));
                }
            }
        }

        if (config.onunload instanceof Function) this.onunload_function_cache.set(name, config.onunload);
        if (config.onguildleave instanceof Function) this.onguildleave_function_cache.push(config.onguildleave);

        // allow modules to capture their own processed config or do whatever after commands were processed
        if (config.onpostload instanceof Function) {
            try {
                if (config.onpostload.constructor.name === 'AsyncFunction') {
                    await config.onpostload(config);
                } else {
                    config.onpostload(config);
                }
            } catch (e) {
                SkittyBot.logger.error(`Module  [${name}]: There was an Error while executing modules onpostload routine.`);
                SkittyBot.logger.reportError(e);
                return false;
            }
        }

        SkittyBot.logger.verbose(`Module  [${name}] finished loading with ${c_count} commands and ${a_count} aliases`);

        return true;

    }

    _depsForPlugin(_path) {
        let pkg;
        try {
            if (!fs.existsSync(_path)) return;
            pkg = require(_path);
            if (pkg.dependencies) {
                for (let name in pkg.dependencies) {
                    // should we be handling a situation where two of the same dependencies exist but have different version requirements?
                    this.dependencies.set(name, pkg.dependencies[name]);
                }
            }
        } catch (e) {
            SkittyBot.logger.log(`Failed loading package.json for ${_path}`);
            SkittyBot.logger.reportError(e);
        }
    }

    _forPath(base) {
        let plugin_folders = SkittyBot.utils.fetch_directories(base);
        for (let i = 0; i < plugin_folders.length; i++) {
            this._depsForPlugin(path.join(base, plugin_folders[i], 'package.json'));
        }
    }

    _buildDependencies() {
        this._forPath(SkittyBot.config.location.public_plugins);
        this._forPath(SkittyBot.config.location.master_plugins);
        this._depsForPlugin(path.join(SkittyBot.config.location.home, 'package.json'));
    }

    /**
     * Concatenates all dependencies from all plugins into the bots main package.json
     * @returns {Promise} true if successful, false otherwise
     * @memberof plug_man
     */

    updatePackageJSON() {
        return new Promise(resolve => {
            this._buildDependencies();
            if (this.dependencies.size === 0) {
                resolve(false);
                return;
            }
            let mainPKG = require(path.join(SkittyBot.config.location.home, 'package.json'));
            let deps = this.dependencies.keys();
            let pkg;
            let dependencies = {};
            for (; ;) {
                pkg = deps.next();
                if (pkg.done) break;
                dependencies[pkg.value] = this.dependencies.get(pkg.value);
            }
            mainPKG.dependencies = dependencies;

            fs.writeFileSync(path.join(SkittyBot.config.location.home, 'package.json'), `${JSON.stringify(mainPKG, null, 2)}\n`);
        });
    }

    registrationFailure(loc, msg) {
        SkittyBot.logger.error(`Failed loading plugin from: ${loc}`);
        SkittyBot.logger.error(`Reason: ${msg}`);
    }

    registerConfiguration(caller, config) {
        let pkg, package_path = path.join(caller, 'package.json');
        if (!fs.existsSync(package_path)) {
            this.registrationFailure(caller, 'missing package.json');
            return;
        }
        try {
            pkg = require(package_path);
        } catch (e) {
            this.registrationFailure(caller, 'invalid or corrupt package.json');
            return;
        }
        // If a plugin's package.json contains the key "disabled" set to true, the plugin will not create any commands in the bot.
        config.enabled = !pkg.disabled;
        // Plugins file path
        config.path = caller;
        // Plugins title and description used in the web app and help command
        if (!pkg.plugin_title) {
            this.registrationFailure(caller, 'package.json missing "plugin_title"');
            return;
        }
        if (!pkg.description) {
            this.registrationFailure(caller, 'package.json missing "description"');
            return;
        }
        if (pkg.plugin_title.length < 3) {
            this.registrationFailure(caller, 'package.json "plugin_title" must be longer than 3 characters');
            return;
        }
        if (pkg.description.length < 3) {
            this.registrationFailure(caller, 'package.json "description" must be longer than 3 characters');
            return;
        }
        config.plugin_title = pkg.plugin_title;
        config.description = pkg.description;
        this._createCommands(config);
    }

    _loadPluginDir(base) {
        let plugin_folders = SkittyBot.utils.fetch_directories(base);
        let pkg;
        for (let i = 0; i < plugin_folders.length; i++) {
            pkg = path.join(base, plugin_folders[i], 'package.json');
            if (fs.existsSync(pkg)) {
                try {
                    require(path.join(base, plugin_folders[i]));
                } catch (e) {
                    SkittyBot.logger.reportError(e);
                }
            }
        }
    }

    loadPlugins() {
        // Always load master first, then private and public.
        this._loadPluginDir(SkittyBot.config.location.master_plugins);
        this._loadPluginDir(SkittyBot.config.location.private_plugins);
        this._loadPluginDir(SkittyBot.config.location.public_plugins);
    }

    unload() {
        for (let key of this.onunload_function_cache.keys()) this.onunload_function_cache.get(key)();
    }
}

module.exports = plug_man;
