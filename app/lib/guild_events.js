'use strict';

const utils = require('./utility');

class guild_events {
    constructor() {
        this.gjc = new Map();
        this.glc = new Map();

        this.gmac = new Map();
        this.gmrc = new Map();

        SkittyBot.client.on(SkittyBot.event.GUILD_CREATE, this.dispatch_guild_join.bind(this));
        SkittyBot.client.on(SkittyBot.event.GUILD_DELETE, this.dispatch_guild_leave.bind(this));

        SkittyBot.client.on(SkittyBot.event.GUILD_MEMBER_ADD, this.dispatch_guild_member_add.bind(this));
        SkittyBot.client.on(SkittyBot.event.GUILD_MEMBER_REMOVE, this.dispatch_guild_member_remove.bind(this));
    }

    dispatch_guild_join(guild) {
        for (let i of this.gjc.keys()) {
            this.gjc.get(i)(guild);
        }
    }

    dispatch_guild_leave(guild) {
        for (let i of this.glc.keys()) {
            this.glc.get(i)(guild);
        }
        if (Array.isArray(SkittyBot.plugins.onguildleave_function_cache) && SkittyBot.plugins.onguildleave_function_cache.length) {
            for (let fn in SkittyBot.plugins.onguildleave_function_cache) SkittyBot.plugins.onguildleave_function_cache[fn]();
        }
    }

    onGuildJoinRegisterCallback(callback) {
        if (!(callback instanceof Function)) throw Error(`onGuildJoin was called with a non "Function" argument.`);
        let id = utils.genID();
        this.gjc.set(id, callback);
        return id;
    }

    onGuildLeaveRegisterCallback(callback) {
        if (!(callback instanceof Function)) throw Error(`onGuildLeave was called with a non "Function" argument.`);
        let id = utils.genID();
        this.glc.set(id, callback);
        return id;
    }

    dispatch_guild_member_add(guild) {
        for (let i of this.gmac.keys()) {
            this.gmac.get(i)(guild);
        }
    }

    dispatch_guild_member_remove(guild) {
        for (let i of this.gmrc.keys()) {
            this.gmrc.get(i)(guild);
        }
    }

    onGuildMemberAddCallback(callback) {
        if (!(callback instanceof Function)) throw Error(`onGuildMemberAdd was called with a non "Function" argument.`);
        let id = utils.genID();
        this.gmac.set(id, callback);
        return id;
    }

    onGuildMemberRemoveCallback(callback) {
        if (!(callback instanceof Function)) throw Error(`onGuildMemberRemove was called with a non "Function" argument.`);
        let id = utils.genID();
        this.gmrc.set(id, callback);
        return id;
    }

    delete(id) {
        this.glc.delete(id);
        this.gjc.delete(id);
        this.gmac.delete(id);
        this.gmrc.delete(id);
    }
}

module.exports = guild_events;
