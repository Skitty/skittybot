'use strict';

const utils = require('./utility');

class message_manager {

    constructor() {

        // make sure bot owner is always a ring 0 executor
        SkittyBot.administrators.set(SkittyBot.config.owner_id, 0);

        this.bot_prefix = SkittyBot.config.bot_prefix;
        if (this.bot_prefix) {
            this.bot_prefix = this.bot_prefix.toLowerCase();
        } else {
            SkittyBot.logger.error(`A required default prefix was not found. Defaulting to "skittybot".`);
            this.bot_prefix = 'skittybot';
        }

        this.commands = SkittyBot.commands;
        this.command_alias = SkittyBot.command_alias;

        this.cache_global_blacklist = false;
        this.cache_user_blacklist = false;

        this.cache_guild_commands_blacklist = new Map();
        this.cache_guild_module_blacklist = new Map();

        this.cache_custom_commands = new Map();
        this.cache_private_enable = new Map();

        this.command_throttle = new Map();

        this.function_callbacks = new Map();
        this.root_callbacks = new Map();
        this.command_callbacks = new Map();

        process.nextTick(this.init.bind(this));

    }

    init() {
        SkittyBot.client.on(SkittyBot.event.MESSAGE_CREATE, this.process.bind(this));
    }

    shut_down() {
        SkittyBot.logger.debug(`Unloading plugins.`);
        SkittyBot.plugins.unload();
    }

    next(a) {
        a.shift();
        return this.first(a);
    }

    first(a) {
        if (a[0]) return a[0].toLowerCase();
        return String();
    }

    printOrigin(m) {
        let guild_name = 'DM';
        if (m.channel.type === 'text') guild_name = m.guild.name;
        SkittyBot.logger.log_command(`[ORIG: ${guild_name}; USR: ${m.author.tag}]\n${m.cleanContent}`);
    }
    /**
     * Register a function call back
     * @param {Function} callback a function to be called with with the default message object
     * @returns {number} callback id
     */
    msgCallback(callback) {
        if (!(callback instanceof Function)) throw Error(`msgCallback was called with a non "Function" paramter.`);
        let id = utils.genID();
        this.function_callbacks.set(id, callback);
        return id;
    }

    rootmsgCallback(callback) {
        if (!(callback instanceof Function)) throw Error(`rootmsgCallback was called with a non "Function" paramter.`);
        let id = utils.genID();
        this.root_callbacks.set(id, callback);
        return id;
    }

    commandCallback(callback) {
        if (!(callback instanceof Function)) throw Error(`commandCallback was called with a non "Function" paramter.`);
        let id = utils.genID();
        this.command_callbacks.set(id, callback);
        return id;
    }

    deleteMsgCallback(id) {
        this.function_callbacks.delete(id);
    }

    deleteRootCallback(id) {
        this.root_callbacks.delete(id);
    }

    deleteCommandCallback(id) {
        this.command_callbacks.delete(id);
    }

    dispatch(msg) {
        for (let i of this.function_callbacks.keys()) {
            this.function_callbacks.get(i)(msg);
        }
    }

    rootDispatch(msg) {
        for (let i of this.root_callbacks.keys()) {
            this.root_callbacks.get(i)(msg);
        }
    }

    commandDispatch(msg, cmd) {
        for (let i of this.command_callbacks.keys()) {
            this.command_callbacks.get(i)(msg, cmd);
        }
    }

    async genGlobalBlacklist() {
        SkittyBot.global_command_blacklist.clear();
        SkittyBot.global_module_blacklist.clear();
        await SkittyBot.system_database.each('SELECT * FROM blacklist_commands WHERE isglobal = 1;', (e, row) => {
            if (e) {
                console.error(e);
                return;
            }
            SkittyBot.global_command_blacklist.set(row.command, true);
        });
        await SkittyBot.system_database.each('SELECT * FROM blacklist_modules WHERE isglobal = 1;', (e, row) => {
            if (e) {
                console.error(e);
                return;
            }
            SkittyBot.global_module_blacklist.set(row.module, true);
        });
        this.cache_global_blacklist = true;
    }

    async genGuildCommandBlacklist(guild_id) {
        let gbl = [];
        await SkittyBot.system_database.each('SELECT * FROM blacklist_commands WHERE guild_id = ?;', guild_id, (e, row) => {
            if (e) {
                console.error(e);
                return;
            }
            if (row.channel_id && row.channel_id !== '0') {
                let cbl = SkittyBot.channel_command_blacklist.get(row.channel_id);
                if (!cbl) {
                    cbl = [];
                    SkittyBot.channel_command_blacklist.set(row.channel_id, cbl);
                }
                cbl.push(row.command);
            } else {
                gbl.push(row.command);
            }
        });
        if (gbl.length) SkittyBot.guild_command_blacklist.set(guild_id, gbl);
        this.cache_guild_commands_blacklist.set(guild_id, true);
    }

    async genGuildModuleBlacklist(guild_id) {
        let gbl = [];
        await SkittyBot.system_database.each('SELECT * FROM blacklist_modules WHERE guild_id = ?;', guild_id, (e, row) => {
            if (e) {
                console.error(e);
                return;
            }
            if (row.channel_id && row.channel_id !== '0') {
                let cbl = SkittyBot.channel_module_blacklist.get(row.channel_id);
                if (!cbl) {
                    cbl = [];
                    SkittyBot.channel_module_blacklist.set(row.channel_id, cbl);
                }
                cbl.push(row.module);
            } else {
                gbl.push(row.module);
            }
        });
        if (gbl.length) SkittyBot.guild_module_blacklist.set(guild_id, gbl);
        this.cache_guild_module_blacklist.set(guild_id, true);
    }

    async genUserBlacklist() {
        SkittyBot.blacklist_user.clear();
        SkittyBot.guild_blacklist_user.clear();
        SkittyBot.channel_blacklist_user.clear();
        await SkittyBot.system_database.each('SELECT * FROM blacklist_user;', (err, row) => {
            if (err) {
                console.error(err);
                return;
            }
            if (row.isglobal) {
                SkittyBot.blacklist_user.set(row.user_id, true);
            } else if (row.channel_id && row.channel_id !== '0') {
                let users = SkittyBot.channel_blacklist_user.get(row.channel_id) || [];
                users.push(row.user_id);
                SkittyBot.channel_blacklist_user.set(row.channel_id, users);
            } else if (row.guild_id) {
                let users = SkittyBot.guild_blacklist_user.get(row.guild_id) || [];
                users.push(row.user_id);
                SkittyBot.guild_blacklist_user.set(row.guild_id, users);
            }
        });
        this.cache_user_blacklist = true;
    }

    async genGuildPrivateEnable(guild_id) {
        let p = [];
        SkittyBot.private_enable.set(guild_id, p);
        await SkittyBot.system_database.each('SELECT * FROM enable_private WHERE guild_id = ?;', guild_id, (err, row) => {
            if (!err) p.push(row.command);
        });
        this.cache_private_enable.set(guild_id, true);
    }

    async genGuildAliases(guild_id) {
        let aliases = new Map();
        SkittyBot.custom_commands.set(guild_id, aliases);
        await SkittyBot.system_database.each('SELECT * FROM custom_commands WHERE guild_id = ?;', guild_id, (err, row) => {
            if (!err) aliases.set(row.alias, { command: row.command, args: row.args });
        });
        this.cache_custom_commands.set(guild_id, true);
    }

    async check_cache(msg) {
        // generate blacklists when required
        if (!this.cache_guild_commands_blacklist.get(msg.guild.id)) {
            await this.genGuildCommandBlacklist(msg.guild.id);
        }
        if (!this.cache_guild_module_blacklist.get(msg.guild.id)) {
            await this.genGuildModuleBlacklist(msg.guild.id);
        }
        if (!this.cache_private_enable.get(msg.guild.id)) {
            await this.genGuildPrivateEnable(msg.guild.id);
        }
        if (!this.cache_custom_commands.get(msg.guild.id)) {
            await this.genGuildAliases(msg.guild.id);
        }
        if (!this.cache_global_blacklist) {
            await this.genGlobalBlacklist();
        }
        if (!this.cache_user_blacklist) {
            await this.genUserBlacklist();
        }
    }

    async process(msg) {

        this.rootDispatch(msg);

        if (msg.author.bot) return;

        if (msg.guild) {
            await this.check_cache(msg);
            // ignore blacklisted guild
            if (SkittyBot.blacklist_guild.get(msg.guild.id)) return;
            // guild wide user block
            let users = SkittyBot.guild_blacklist_user.get(msg.guild.id);
            if (Array.isArray(users) && users.includes(msg.author.id)) return;
            // channel specific user block
            users = SkittyBot.channel_blacklist_user.get(msg.channel.id);
            if (Array.isArray(users) && users.includes(msg.author.id)) return;
        }

        if (SkittyBot.blacklist_user.get(msg.author.id)) return;

        let tokens = msg.content.trim().split(/\s+/g);

        let token = this.first(tokens);

        // Absolute minimum requirement for a message to be a command
        if (token.length < 2 && tokens.length < 2) return;

        let guild_prefix = this.bot_prefix;
        let user_prefix;

        if (msg.guild && SkittyBot.guild_prefix.has(msg.guild.id)) {
            guild_prefix = SkittyBot.guild_prefix.get(msg.guild.id);
        }

        if (SkittyBot.user_prefix.has(msg.author.id)) {
            user_prefix = SkittyBot.user_prefix.get(msg.author.id);
        }

        let struck = false;

        // handle any possible means of command invocation
        if (token.includes(SkittyBot.client.user.id) && token.startsWith('<') && token.endsWith('>')) {
            // the bot is being @mentioned in order to invoke a command
            struck = true;
            token = this.next(tokens);
            // hack: we remove the bot from mentions user map if it is not the target of a command
            // this is done in order to reduce complexity of certain commands that rely on "msg.mentions.users" functions
            // or to avoid interference with commands that rely on mentions
            // this may still have adverse effects on commands that were executed with the bot mention but also included the bot mention at the end of a command...
            let me = tokens.filter(str => str.includes(SkittyBot.client.user.id));
            if (me.length === 0 && msg.mentions.users.has(SkittyBot.client.user.id)) {
                msg.mentions.users.delete(SkittyBot.client.user.id);
            }
        } else if (token.startsWith(this.bot_prefix)) {
            struck = true;
            token = token.slice(this.bot_prefix.length);
        } else if (guild_prefix && token.startsWith(guild_prefix)) {
            struck = true;
            token = token.slice(guild_prefix.length);
        } else if (user_prefix && token.startsWith(user_prefix)) {
            struck = true;
            token = token.slice(user_prefix.length);
        }

        if (!struck) {
            // not a command so send msg to anything else that might handle this
            this.dispatch(msg);
            return;
        }

        if (!token.length) token = this.next(tokens);
        // current token is command
        tokens.shift();
        let command = token;
        let command_object;
        let silent = false;

        if (!command) {
            silent = true;
            command = 'chat';
        }

        if (command && msg.guild && SkittyBot.custom_commands.size > 0 && SkittyBot.custom_commands.has(msg.guild.id)) {
            // there are possibly custom commands we could handle
            let custom_commands = SkittyBot.custom_commands.get(msg.guild.id);
            let command_alias_object;
            if (custom_commands && custom_commands.size > 0 && custom_commands.has(command)) {
                command_alias_object = custom_commands.get(command);
                command_object = SkittyBot.getCommand(command_alias_object.command);
                if (command_object) {
                    if (tokens.length === 0) {
                        // custom command alias has default args, so set them when none were provided
                        tokens = command_alias_object.args.split(' ');
                    }
                }
            }
        }

        if (!command_object) command_object = SkittyBot.getCommand(command);
        // activate chat
        // if (!command_object) {
        //     silent = true;
        //     tokens.unshift(command);
        //     command_object = SkittyBot.getCommand('chat');
        // }

        if (command_object) {
            if (!command_object.hasOwnProperty('process')) return;

            // capture incorrect cases
            if (command_object.hasOwnProperty('guildonly')) {
                command_object.guildOnly = command_object.guildonly;
            }
            if (command_object.hasOwnProperty('nodisable')) {
                command_object.noDisable = command_object.nodisable;
            }

            if (this.canExecute(msg, command, command_object, silent)) {
                this.printOrigin(msg);
                this.commandDispatch(msg, command_object);
                if (command_object.process.constructor.name === 'AsyncFunction') {
                    command_object.process(msg, tokens)
                        .catch(e => {
                            SkittyBot.logger.error(`Module  [${command_object.plugin_title}] RAISED AN ERROR`);
                            SkittyBot.logger.reportError(e);
                        });
                } else {
                    try {
                        command_object.process(msg, tokens);
                    } catch (error) {
                        SkittyBot.logger.error(`Module  [${command_object.plugin_title}] RAISED AN ERROR`);
                        SkittyBot.logger.reportError(error);
                    }
                }
            }
        }
    }

    throttle_check(id, command, command_object) {
        if (!this.command_throttle.has(command)) {
            const data = new Map();
            this.command_throttle.set(command, data);
        }
        let now = +new Date();
        let cd = this.command_throttle.get(command);
        if (!cd.has(id)) {
            cd.set(id, now);
            return false;
        }
        let lastUsage = cd.get(id) + command_object.throttle;
        if (lastUsage > now) return Math.ceil((lastUsage - now) / 1000);
        cd.set(id, now);
        return false;
    }

    canExecute(msg, command, command_object, silent = false) {

        if (SkittyBot.DEVOVERRIDE && SkittyBot.DEVOVERRIDE === 0xA5501) {
            // if the highest elevated bot admin enabled the developer override, they can execute anything
            if (SkittyBot.config.owner_id === msg.author.id) return true;
        }

        if (!SkittyBot.administrators.has(msg.author.id) && SkittyBot.global_command_blacklist.get(command)) {
            if (!silent) {
                msg.channel.send(`Sorry, but my developer temporarily disabled this command.`)
                    .then(m => m.delete({ timeout: 10000 }));
            }
            return false;
        }

        if (!SkittyBot.administrators.has(msg.author.id) && SkittyBot.global_module_blacklist.size &&
            SkittyBot.global_module_blacklist.get(command_object.plugin_title.toLowerCase())) {
            if (!silent) {
                msg.channel.send(`Sorry, but my developer temporarily disabled "${command_object.plugin_title}" commands for now.`)
                    .then(m => m.delete({ timeout: 10000 }));
            }
            return false;
        }

        if (command_object.private && !SkittyBot.administrators.has(msg.author.id)) {
            let commands = SkittyBot.private_enable.get(msg.guild.id);
            if (!Array.isArray(commands)) return false;
            if (!commands.includes(command_object.command)) return false;
        }

        if (msg.guild) {
            let command_bl = SkittyBot.guild_command_blacklist.get(msg.guild.id);
            if (Array.isArray(command_bl) && command_bl.includes(command_object.command)) {
                if (!silent) {
                    msg.channel.send(`Sorry, but a guild manager disabled that command in this guild.`)
                        .then(m => m.delete({ timeout: 10000 }));
                }
                return false;
            }
            command_bl = SkittyBot.channel_command_blacklist.get(msg.channel.id);
            if (Array.isArray(command_bl) && command_bl.includes(command_object.command)) {
                if (!silent) {
                    msg.channel.send(`Sorry, but a guild manager disabled that command in this channel.`)
                        .then(m => m.delete({ timeout: 10000 }));
                }
                return false;
            }
            let module_bl = SkittyBot.guild_module_blacklist.get(msg.guild.id);
            if (Array.isArray(module_bl) && module_bl.includes(command_object.plugin_title.toLowerCase())) {
                if (!silent) {
                    msg.channel.send(`Sorry, but "${command_object.plugin_title}" commands were disabled in this guild by a guild manager.`)
                        .then(m => m.delete({ timeout: 10000 }));
                }
                return false;
            }
            module_bl = SkittyBot.channel_module_blacklist.get(msg.channel.id);
            if (Array.isArray(module_bl) && module_bl.includes(command_object.plugin_title.toLowerCase())) {
                if (!silent) {
                    msg.channel.send(`Sorry, but "${command_object.plugin_title}" commands were disabled in this channel by a guild manager.`)
                        .then(m => m.delete({ timeout: 10000 }));
                }
                return false;
            }
            let bot_channel_perms = msg.channel.permissionsFor(SkittyBot.client.user.id);
            if (!bot_channel_perms.has('SEND_MESSAGES')) {
                // try to advise when permissions are set up incorrectly
                if (!silent) {
                    msg.author.send([
                        `I don't have permission to post in that channel.`,
                        `This is because a guild owner or administrator has revoked my \`SEND_MESSAGES\` permissions in ${msg.guild.name}#${msg.channel.name}!`
                    ].join('\n'))
                        .catch(() => { SkittyBot.logger.error(`Failed sending permission denied warning to user.`); });
                }
                return false;
            }
            if (!bot_channel_perms.has('EMBED_LINKS')) {
                // We have the required SEND_MESSAGES perm, but we make extensive use of EMBED_LINKS, so make it a requirement by default...
                if (!silent) {
                    msg.channel.send([
                        `Sorry, but my permission are set up incorrectly...`,
                        `In order to function as intended, I require the \`EMBED_LINKS\` permission.`,
                        `A large number of my commands rely on my ability to send embeds to a channel. Without it, a lot of commands will appear to be broken due to this misconfiguration.`
                    ].join('\n'))
                        .catch(() => { SkittyBot.logger.error(`Failed sending permission denied warning to user.`); });
                }
                return false;
            }
        }

        if (command_object.throttle) {
            if (!SkittyBot.administrators.has(msg.author.id)) {
                let cd = this.throttle_check(msg.author.id, command, command_object);
                if (cd && !silent) {
                    msg.reply(`you'll have to wait ${cd} more ${cd !== 1 ? 'seconds' : 'second'} for this command.`)
                        .then(m => m.delete({ timeout: 10000 }));
                    return false;
                }
            }
        }

        if (typeof command_object.enabled === 'boolean' && command_object.enabled === false) {
            if (!silent) {
                msg.channel.send(`Sorry, but my developer has temporarily disabled this command.`)
                    .then(m => m.delete({ timeout: 30000 }));
            }
            return false;
        }

        if (command_object.guildOnly && !msg.guild) {
            if (!silent) {
                msg.channel.send(`This command is restricted to guild channels.`);
            }
            return false;
        }

        if (command_object.owner) {
            if (!SkittyBot.administrators.has(msg.author.id)) {
                if (!silent) {
                    msg.channel.send(`I've been set to only allow bot administrators to run this. Sorry...`)
                        .then(m => m.delete({ timeout: 10000 }));
                }
                return false;
            } else {
                let ring = SkittyBot.administrators.get(msg.author.id);
                // if the required ring is a higher permission level than what the current admin has then deny the command
                if (isNaN(command_object.ring) || (!isNaN(command_object.ring) && (command_object.ring < 0 || command_object.ring > 3))) {
                    if (!silent) {
                        msg.channel.send([
                            `This commands execution ring has not been properly configured.`,
                            `Please advise the bot owner about this error if you believe it to be a mistake.`
                        ].join('\n'));
                    }
                    return false;
                }
                if (command_object.ring < ring) {
                    if (!silent) {
                        msg.channel.send(`You attempted to execute a ring ${command_object.ring} command but your execution elevation is set to ${ring}.`)
                            .then(m => m.delete({ timeout: 10000 }));
                    }
                    return false;
                }
            }
        }

        /*
            Does the bot have the permissions too?
        */

        if (command_object.client && msg.guild) {
            let bot_perms = msg.guild.me.permissionsIn(msg.channel.id);
            if (!bot_perms.has(command_object.client)) {
                if (!silent) {
                    let perms = bot_perms.missing(command_object.client);
                    if (Array.isArray(perms)) perms = perms.join(', ');
                    msg.channel.send([
                        `The command you are attempting to use requires that I have channel permissions that are currently denied to my role in this channel or guild.`,
                        `These are the missing permissions my role requires in order to use this command without error: \`${perms}\``
                    ].join('\n'));
                }
                return false;
            }
        }

        /*
            Does the author have permission?
        */

        if (command_object.member && msg.guild) {
            let user_perms = msg.member.permissionsIn(msg.channel.id);
            if (!user_perms.has(command_object.member)) {
                if (!silent) {
                    let perms = user_perms.missing(command_object.member);
                    if (Array.isArray(perms)) perms = perms.join(', ');
                    msg.channel.send([
                        `The command you're trying to use requires that you have special permissions granted to your role by a guild owner or administrator.`,
                        `Your missing role permissions are as follows: \`${perms}\``
                    ].join('\n'))
                        .then(m => m.delete({ timeout: 15000 }));
                }
                return false;
            }
        }

        /*
            Don't execute NSFW commands outside of a NSFW channel or DM.
            NSFW  commands will not be listed outside of a NSFW channel.
        */

        if (command_object.nsfw) {
            // TODO: Add some command that lets a user trigger whether they're alright with NSFW commands in DMs
            // Currently NSFW commands are enabled by default in DM channels
            if (msg.guild && !msg.channel.nsfw) {
                if (!silent) {
                    msg.channel.send(`NSFW commands are for NSFW channels only.`);
                }
                return false;
            }
        }

        return true;
    }

}

module.exports = message_manager;
