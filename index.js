'use strict';

process.title = 'skittybot';

// const NETWORK_ERRORS = ['ETIMEDOUT', 'ENOTFOUND', 'ECONNREFUSED', 'ECONNRESET'];
const SIGNALS = ['SIGTERM', 'SIGHUP', 'SIGINT', 'SIGBREAK', 'SIGQUIT'];

// function isNetworkError(error) {
//     if (typeof error.code) {
//         if (NETWORK_ERRORS.indexOf(error.code) !== -1) {
//             return true;
//         }
//     }
//     return false;
// }

function handleUncaughtError(error) {
    // if (isNetworkError(error)) {
    //     return;
    // }
    let message, ref, stack;
    stack = (ref = error.stack) !== null ? ref : `${error.name}: ${error.message}`;
    message = `Uncaught Exception:\n  ${stack}`;
    console.error(`An uncaught exception occurred in the main process ${message}`);
}

process.on('uncaughtException', handleUncaughtError);

// process.setUncaughtExceptionCaptureCallback(handleUncaughtError);

process.on('unhandledRejection', handleUncaughtError);

process.on('warning', warning => console.warn(warning.stack));

if (process.platform !== 'win32') {
    console.warn(`This program has not been tested on this platform!`);
}

// Exit cleanly on signals
SIGNALS.forEach(signal => {
    process.on(signal, () => {
        SkittyBot.shut_down();
    });
});

require('./app/skittybot');
