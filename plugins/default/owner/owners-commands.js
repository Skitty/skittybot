'use strict';

const { MessageEmbed } = require('discord.js');
const moment = require('moment');
const util = require('util');
const path = require('path');

const { image_fetcher, param } = require(path.join(SkittyBot.config.location.lib, 'utility.js'));

let announcements = null;

class guild_events {
    constructor() {
        this.modlog_channels = [];
    }
    addChannel(id) {
        if (!this.modlog_channels.includes(id)) {
            this.modlog_channels.push(id);
        }
    }
    removeChannel(id) {
        let index = this.modlog_channels.indexOf(id);
        if (~index) this.modlog_channels.splice(index, 1);
    }
    join(guild) {
        this.dispatch(
            this.genEmbed(guild)
                .setColor(0x00FF00)
                .setTitle(`Joined a guild: ${guild.name}`)
        );
    }
    leave(guild) {
        this.dispatch(
            this.genEmbed(guild)
                .setColor(0xFF0000)
                .setTitle(`Left guild: ${guild.name}`)
        );
    }
    dispatch(embed) {
        let channel;
        for (let i in this.modlog_channels) {
            channel = SkittyBot.client.channels.get(this.modlog_channels[i]);
            if (channel) channel.send(embed);
        }
    }
    genEmbed(guild) {
        let tag = '```';
        let info = [];
        let humans = guild.members.cache.filter(u => !u.user.bot).size;
        let bots = guild.members.cache.filter(u => u.user.bot).size;
        let online = guild.members.cache.filter(u => u.user.presences !== 'offline').size;

        let highest = guild.roles.cache.sort((a, b) => a.position - b.position || a.id - b.id).last();
        if (highest && highest.name) {
            highest = highest.name;
        } else {
            highest = `N/A`;
        }

        info.push(`ID                : ${guild.id}`);
        info.push(`Owner             : ${guild.owner ? guild.owner.user.tag : 'unknown'} (ID: ${guild.owner ? guild.owner.id : 'unknown'})`);
        info.push(`Created           : ${moment(guild.createdAt).format('MMMM Do YYYY')}`);
        info.push(`Region            : ${guild.region}`);
        info.push(`Member count      : ${guild.members.size} [online: ${online}]`);
        info.push(`Humans            : ${humans} [${((humans / guild.members.cache.size) * 100).toFixed(1)}%]`);
        info.push(`Bots              : ${bots} [${((bots / guild.members.cache.size) * 100).toFixed(1)}%]`);
        info.push(`Highest Role      : ${highest}`);
        info.push(`Content Filter    : ${['Disabled', 'Users without role', 'Everyone'][guild.explicitContentFilter]}`);
        info.push(`Verification Level: ${['None', 'Low', 'Medium', '(╯°□°）╯︵ ┻━┻', '┻━┻ ﾐヽ(ಠ益ಠ)ノ彡┻━┻'][guild.verificationLevel]}`);

        return new MessageEmbed()
            .setThumbnail(guild.iconURL({ format: 'jpg', size: 2048 }))
            .setDescription(`${tag}\n${info.join('\n')}${tag}`)
            .setTitle(guild.name);
    }
}

const guildEvents = new guild_events();

class announcement_handler {
    constructor() {

        this.db = SkittyBot.db;
        this.database = null;

        this.max_messages = 10;
        this.messages = new Map();

        // share the announcement map with the global object to easily be used elsewhere
        SkittyBot.announcements = this.messages;

        process.nextTick(this.init.bind(this));
    }
    async init() {

        this.database = await this.db.open(path.join(SkittyBot.config.location.database, 'changelog.db'));

        await this.database.exec(`

            CREATE TABLE IF NOT EXISTS announcements (
                id                  INTEGER PRIMARY KEY,
                message             TEXT NOT NULL,
                logged              INTEGER NOT NULL
            );

        `);

        this.create_cache();

    }

    create_cache() {
        this.messages.clear();
        this.database.each(`SELECT * FROM announcements ORDER BY id DESC LIMIT ?;`, this.max_messages, (err, row) => {
            if (err) return;
            this.messages.set(row.id, {
                message: row.message,
                logged: row.logged
            });
        });
    }

    command_handler(msg, args) {
        let command = `${args.shift()}`.toLocaleLowerCase();
        let id, logged = +new Date(), message = args.join(' ');
        switch (command) {
            case 'create':
            case 'new': {
                this.database.run('INSERT INTO announcements (message, logged) VALUES (?, ?);', message, logged)
                    .then(res => {
                        if (res.changes) {
                            msg.react('👌');
                            this.create_cache();
                        }
                    })
                    .catch(err => {
                        console.error(err);
                        msg.reply(`Something went wrong while doing that...`);
                    });
                break;
            }
            case 'delete':
            case 'remove':
            case 'clear': {
                id = parseInt(message);
                if (isNaN(id)) {
                    msg.reply(`Use the command \`removelast\` to remove the last created message without using the message ID.`);
                    return;
                }
                this.database.run('DELETE FROM announcements WHERE id = ?;', id)
                    .then(res => {
                        if (res.changes) {
                            msg.react('👌');
                            this.create_cache();
                        } else {
                            msg.reply(`This message wasn't in the database.`);
                        }
                    })
                    .catch(err => {
                        console.error(err);
                        msg.reply(`There was an error...`);
                    });
                break;
            }
            case 'remove-last':
            case 'removelast': {
                id = this.messages.keys();
                if (id) {
                    id = id.next();
                }
                if (id) {
                    id = id.value;
                }
                if (!id) {
                    msg.reply(`Nothing to remove.`);
                    return;
                }
                this.database.run('DELETE FROM announcements WHERE id = ?;', id)
                    .then(res => {
                        if (res.changes) {
                            msg.react('👌');
                            this.create_cache();
                        } else {
                            msg.reply(`This channel wasn't in the database.`);
                        }
                    })
                    .catch(err => {
                        console.error(err);
                        msg.reply(`Oopsie, I had an error! s-sowwy~`);
                    });
                break;
            }
            default: msg.reply('Commands are: `create|new` and `delete|remove|clear <id>`');
        }

    }

    async destroy() {
        await this.database.close();
    }

}

async function bot_evaluate(msg, args) {

    if (SkittyBot.config.owner_id !== msg.author.id && SkittyBot.DEVOVERRIDE !== 0xA5501) {
        msg.reply(`I can't let you do that.`);
        return;
    }

    let input = args.join(' ');
    const silent = input.includes('--silent');
    const asynchr = input.includes('--async');
    if (silent || asynchr) {
        input = input.replace(/--silent|--async/g, '');
    }

    let result;
    try {
        result = asynchr ? eval(`(async()=>{${input}})();`) : eval(input);
        if (result instanceof Promise && asynchr) {
            result = await result;
        }
        const tokenRegex = new RegExp(SkittyBot.client.token, 'gi');
        if (typeof result !== 'string') {
            result = util.inspect(result, { depth: 0 });
        }
        result = result.replace(tokenRegex, '<CLIENT-TOKEN>');
    } catch (err) {
        result = err.message;
    }

    if (!silent) {
        msg.channel.send(`\`\`\`js\n${result}\n\`\`\``);
    } else {
        msg.delete();
    }
}

async function bot_blacklist_guild(msg, args) {
    if (args[0]) {
        let blacklist = SkittyBot.blacklist_guild.get(args[0]) === undefined;
        let guild = SkittyBot.client.guilds.get(args[0]);
        if (blacklist) {
            SkittyBot.blacklist_guild.set(args[0], blacklist);
            SkittyBot.system_database.run('INSERT INTO blacklist_guild (guild_id) VALUES (?);', args[0])
                .then(() => {
                    if (guild) {
                        msg.channel.send(`Ok, I'll ignore everyone in [${guild.name}] now.`);
                    } else {
                        msg.channel.send(`Ok, I wont work there anymore!`);
                    }
                })
                .catch(err => {
                    console.error(err);
                    msg.channel.send(`Oh no! Something went wrong. There's more information in my console if you'd have a look.`);
                });

        } else {
            SkittyBot.blacklist_guild.delete(args[0]);
            await SkittyBot.system_database.run('DELETE FROM blacklist_guild WHERE guild_id = ?;', args[0])
                .then(() => {
                    if (guild) {
                        msg.channel.send(`Ok, sure, I wont ignore everyone in [${guild.name}] anymore.`);
                    } else {
                        msg.channel.send(`Ok, that guild isn't on my blacklist anymore!`);
                    }
                })
                .catch(err => {
                    console.error(err);
                    msg.channel.send(`Oh no! Something went wrong. There's more information in my console if you'd have a look.`);
                });
        }
    }
}

async function bot_toggle_private_command(msg, args) {

    let guild_id = param('guild_id', msg.content) || param('gid', msg.content) || param('guildid', msg.content);
    let is_owner = SkittyBot.administrators.has(msg.author.id);

    if (!guild_id) guild_id = msg.guild.id;

    let command, command_object, alias = param('command', msg.content);
    if (!alias) alias = args.pop();
    command = alias;
    if (!command) {
        msg.reply(`You must specify a private command to toggle.`);
        return;
    } else {
        alias = alias.toLowerCase();
        alias = SkittyBot.command_alias.get(alias);
        if (alias !== undefined) {
            command_object = SkittyBot.commands.get(alias);
            command = alias;
        }
        if (!command_object) {
            command = command.toLowerCase();
            command_object = SkittyBot.commands.get(command);
        }
        if (!command_object) {
            msg.reply(`The string "${command}" does not appear to be a command.`);
            return;
        }
        if (command_object.owner && !is_owner) return;
        if (!command_object.private) {
            msg.reply(`The command "${command}" is not private.`);
            return;
        }
    }

    let commands = SkittyBot.private_enable.get(guild_id);
    let enable = !~commands.indexOf(command);
    if (enable) {
        if (commands !== undefined) {
            commands.push(command);
        } else {
            SkittyBot.private_enable.set(guild_id, [command]);
        }
        SkittyBot.system_database.run('INSERT INTO enable_private (guild_id, command) VALUES (?, ?);', guild_id, command)
            .then(() => {
                msg.reply(`Ok. Command ${command} now enabled.`);
            })
            .catch(err => {
                console.error(err);
                msg.reply(`Oh no! Something went wrong.`);
            });

    } else {
        if (commands === undefined) {
            msg.reply(`The command ${command} was not in the command list.`);
            return;
        }
        let index = commands.indexOf(command);
        if (~index) {
            commands.splice(index, 1);
            await SkittyBot.system_database.run('DELETE FROM enable_private WHERE guild_id = ? AND command = ?;', guild_id, command)
                .then(() => {
                    msg.reply(`Ok. Private command "${command}" was disabled.`);
                })
                .catch(err => {
                    console.error(err);
                    msg.reply(`Oh no! Something went wrong.`);
                });
        } else {
            msg.reply(`The command ${command} was not in the command blacklist.`);
        }
    }
}

async function bot_set_avatar(msg, args) {
    // TODO: figure out some way that it is impossible to go past any image, e.g, there was an image but it's too big, don't get the next one after that

    let user_id, user, pic;

    if (args[0]) {
        user_id = /\d{10,}/.exec(args[0]);
        if (Array.isArray(user_id)) user_id = user_id[0];
        user = msg.guild.members.get(user_id);
        if (user) {
            user = user.user;
            pic = user.displayAvatarURL({
                format: 'png',
                size: 256
            });
        }
    }

    let fetcher = new image_fetcher({
        fileSize: 5e+6,
        dimension: 5000,
        mimes: {
            png: true,
            jpg: true,
            jpeg: true,
            gif: true
        }
    });

    let img;

    if (!pic) {
        img = await fetcher.findFirst(msg, args);
        if (!img) {
            msg.channel.send(`Couldn't find a pic.`);
            return;
        }
        pic = img.body;
    }

    SkittyBot.client.user.setAvatar(pic)
        .then(() => msg.reply(`Ok.`))
        .catch(err => {
            console.error(err);
            return msg.reply(`I couldn't do that.`);
        });
}

function bot_set_username(msg, args) {

    let newname = args.join(' ');

    if (newname.length < 2) {
        return;
    }

    SkittyBot.client.user.setUsername(newname)
        .then(() => msg.reply(`Ok.`))
        .catch(err => {
            console.error(err);
            return msg.reply(`I couldn't do that.`);
        });
}

function bot_leave_guild(msg, args) {
    if (!args[0]) return;
    let guild = SkittyBot.client.guilds.get(args[0]);
    if (guild !== undefined) {
        guild.leave()
            .then(g => {
                if (guild.id !== g.id) msg.channel.send(`Left guild ${g.name}`);
            }).catch(console.error);
    }
}

function bot_set_event_log_channel(msg, args) {
    let channel_id = msg.channel.id;
    switch (String(args[0]).toLocaleLowerCase()) {
        case 'enable':
        case 'true':
        case 'on':
            SkittyBot.system_database.run('INSERT OR IGNORE INTO event_log_channels (channel_id) VALUES (?);', channel_id)
                .then(res => {
                    if (res.changes) {
                        msg.react('👌');
                        guildEvents.addChannel(channel_id);
                    } else {
                        msg.reply(`This channel was already defined as the event log channel.`);
                    }
                })
                .catch(err => {
                    console.error(err);
                    msg.reply(`Oh no! Something went wrong.`);
                });
            break;
        case 'disable':
        case 'false':
        case 'off':
            SkittyBot.system_database.run('DELETE FROM event_log_channels WHERE channel_id = ?;', channel_id)
                .then(res => {
                    if (res.changes) {
                        msg.react('👌');
                        guildEvents.removeChannel(channel_id);
                    } else {
                        msg.reply(`This channel wasn't in the database.`);
                    }
                })
                .catch(err => {
                    console.error(err);
                    msg.reply(`Oh no! Something went wrong.`);
                });
            break;
        default:
            msg.reply('Example: `boteventlog enable`');
    }
}

const configuration = {
    onload: () => {
        SkittyBot.onGuildJoin(guildEvents.join.bind(guildEvents));
        SkittyBot.onGuildLeave(guildEvents.leave.bind(guildEvents));
        SkittyBot.system_database.each('SELECT channel_id FROM event_log_channels;', (err, row) => {
            if (!err) guildEvents.addChannel(row.channel_id);
        });
        announcements = new announcement_handler();
    },
    onunload: () => {
        announcements.destroy();
    },
    commands: [
        {
            command: 'exec',
            example: 'exec 1+1',
            description: `Evaluate some javascript in the bot.`,
            aliases: [
                'ev',
                'eval',
                'evaluate',
                'execute'
            ],
            owner: true,
            ring: 0,
            process: bot_evaluate
        },
        {
            command: 'bot-announce',
            example: [
                'bot-announce new the message',
                'bot-announce delete 1',
                'bot-announce remove-last'
            ],
            description: `Create and manage announcements for the bot, these will appear in the help command and announcements command. Only last two are shown.`,
            aliases: [
                'sbannounce',
                'botannounce',
                'announce'
            ],
            owner: true,
            ring: 3,
            process: (msg, args) => {
                announcements.command_handler(msg, args);
            }
        },
        {
            command: 'leave',
            example: 'leave 340951991050043394',
            description: `Makes the bot leave a guild.`,
            owner: true,
            ring: 3,
            process: bot_leave_guild
        },
        {
            command: 'setavatar',
            description: 'Sets the bots avatar. It will grab an image that was previously posted or take a URL to an image.',
            owner: true,
            ring: 1,
            aliases: [
                'setpfp',
                'setav',
                'stealav',
                'stealpfp'
            ],
            process: bot_set_avatar
        },
        {
            command: 'setname',
            description: 'Sets the bots name.',
            owner: true,
            ring: 1,
            aliases: [
                'changename',
                'rename',
                'newname',
                'nameset',
                'username',
                'setusername',
                'usernameset'
            ],
            process: bot_set_username
        },
        {
            command: 'boteventchannel',
            example: [
                `bec enable`,
                `bec disable`,
                `bec true`,
                `bec false`
            ],
            description: `Sets or un-sets the channel as the bots event log channel. This is where the bot posts guild joins and leaves.`,
            owner: true,
            ring: 2,
            aliases: [
                'bec',
            ],
            process: bot_set_event_log_channel
        },
        {
            command: 'blacklistguild',
            aliases: [
                'ignoreguild',
                'ignore-guild'
            ],
            example: 'blg 340951991050043394',
            description: `Adds the guild ID to a blacklist, preventing execution of any commands from that guild.`,
            owner: true,
            ring: 3,
            process: bot_blacklist_guild
        },
        {
            command: 'toggleprivatecommand',
            aliases: [
                'tpc'
            ],
            description: [
                `Disables or enables a private command in the current guild.`,
                `Only the bot owner can use this command.`
            ],
            example: [
                'tpc privatecommandname',
                'tpc -command privatecommandname -gid 164171368517206016'
            ],
            guildOnly: true,
            owner: true,
            ring: 1,
            process: bot_toggle_private_command
        }
    ]
};

SkittyBot.registerPlugin(configuration);
